﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Expression: ");
                var str = Console.ReadLine();
                var arr = PostfixFormConverter.ConvertToPostfix(str);
                if (arr == null)
                {
                    Console.WriteLine(@"Invalid expression");
                    continue;
                }
#if DEBUG
                Print(arr);
#endif
                var res = CalcExpressionHelper.CalcExpression(arr);
                if (res == null)
                {
                    Console.WriteLine(@"Invalid calculation");
                    continue;
                }
                Console.WriteLine($@">: {res}");
            }
        }
#if DEBUG
        static void Print(string[] arr)
        {
            Console.Write(">: ");
            foreach (var i in arr)
                Console.Write(i + " ");
            Console.WriteLine();
        }
#endif
    }
}
