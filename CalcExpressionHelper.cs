﻿using System;
using System.Linq;

namespace Calculator
{
    public static class CalcExpressionHelper
    {
        public static double? CalcExpression(string infixExpression)
        {
            var arr = PostfixFormConverter.ConvertToPostfix(infixExpression);
            if (arr == null)
                return null;
            return CalcExpression(arr);
        }

        public static double? CalcExpression(string[] postfixExpression)
        {
            if (postfixExpression == null || postfixExpression.Length == 0)
                return null;
            string[] buf = new string[postfixExpression.Length];
            Array.Copy(postfixExpression, buf, postfixExpression.Length);
            while (buf.Length > 1)
            {
                var firstOperationIndex = GetFirstOperationIndex(buf);
                if (firstOperationIndex == 0)
                {
                    if (buf.Length == 1)
                        return long.Parse(buf.First());
                    else
                        return null;
                }
                else if (firstOperationIndex == 1)
                {
                    if (buf.Length == 2 && (buf[1].First() == '+' || buf[1].First() == '-'))
                        return buf[1].First() == '+' ? long.Parse(buf[0]) : -long.Parse(buf[0]);
                    else
                        return null;
                }
                else
                {
                    var errorFlag = false;
                    double a = 0, b = 0, res = 0;
                    char op = buf[firstOperationIndex].First();
                    errorFlag = errorFlag || !double.TryParse(buf[firstOperationIndex - 2], out a);
                    errorFlag = errorFlag || !double.TryParse(buf[firstOperationIndex - 1], out b);
                    errorFlag = errorFlag || !Constants.AllowPostfixOperations.Contains(op);
                    if (errorFlag)
                        return null;
                    switch (op)
                    {
                        case '+':
                            res = a + b;
                            break;
                        case '-':
                            res = a - b;
                            break;
                        case '*':
                            res = a * b;
                            break;
                        case '/':
                            res = a / b;
                            break;
                    }
                    buf[firstOperationIndex - 2] = string.Empty;
                    buf[firstOperationIndex - 1] = string.Empty;
                    buf[firstOperationIndex - 0] = res.ToString("0.####");
                    buf = buf.Where(i => i != string.Empty).ToArray();
                }
            }
            return double.Parse(buf[0]);
        }

        #region Private

        private static int GetFirstOperationIndex(string[] postfixExpression)
        {
            var firstOperationIndex = 0;
            for (int i = 0; i < postfixExpression.Length; ++i)
            {
                if (postfixExpression[i].Length == 1 && Constants.AllowPostfixOperations.Contains(postfixExpression[i].First()))
                {
                    firstOperationIndex = i;
                    break;
                }
            }
            return firstOperationIndex;
        }

        #endregion
    }
}
