﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public static class PostfixFormConverter
    {
        /// <summary>
        /// Convert infix form to postfix
        /// </summary>
        public static string[] ConvertToPostfix(string infixForm)
        {
            if (string.IsNullOrWhiteSpace(infixForm))
                return null;
            var preparedSrc = PreparationOfExpression(infixForm);
            if (!FirstCheckExpression(preparedSrc))
                return null;

            var arrayOfLexem = preparedSrc.SplitExt(Constants.AllowInfixOperations);
            var stackOfOperations = new Stack<string>();
            var resoult = new List<string>();

            arrayOfLexem = ConvertUnarnOperators(arrayOfLexem);
            //if (arrayOfLexem.First().Length == 1 && (arrayOfLexem.First().First() == '+' || arrayOfLexem.First().First() == '-'))
            //{
            //    arrayOfLexem[1] = arrayOfLexem.First() + arrayOfLexem[1];
            //    arrayOfLexem = arrayOfLexem.Skip(1).ToArray();
            //}

            foreach (var lexem in arrayOfLexem)
            {
                long tmp;
                // this is constant
                if (long.TryParse(lexem, out tmp))
                    resoult.Add(lexem);
                // this is operation
                else
                {
                    if (lexem == "(")
                        stackOfOperations.Push(lexem);
                    else if (lexem == ")")
                    {
                        while (stackOfOperations.Count != 0 && stackOfOperations.First() != "(")
                            resoult.Add(stackOfOperations.Pop());
                        if (stackOfOperations.Count != 0)
                            stackOfOperations.Pop();
                    }
                    else if (stackOfOperations.Count == 0 || stackOfOperations.First() == "(")
                        stackOfOperations.Push(lexem);
                    else if (GetPriority(lexem) > GetPriority(stackOfOperations.First()))
                        stackOfOperations.Push(lexem);
                    else if (GetPriority(lexem) <= GetPriority(stackOfOperations.First()))
                    {
                        while (stackOfOperations.Count != 0 && stackOfOperations.First() != "(" && GetPriority(stackOfOperations.First()) >= GetPriority(lexem))
                            resoult.Add(stackOfOperations.Pop());
                        stackOfOperations.Push(lexem);
                    }
                    else
                        return null;
                }
            }

            while (stackOfOperations.Count != 0)
                resoult.Add(stackOfOperations.Pop());

            if (resoult.Any(i => i == "(" || i == ")"))
                return null;
            return resoult.ToArray();
        }

        #region Private

        private static string[] ConvertUnarnOperators(string[] lexems)
        {
            var buf = new string[lexems.Length];
            Array.Copy(lexems, buf, lexems.Length);

            if (IsPlusOrMinusOperator(buf.First()))
            {
                if (buf.First().First() == '-')
                    buf = buf.Skip(1).Prepend("*").Prepend("-1").ToArray();
                else
                    buf = buf.Skip(1).ToArray();
            }

            for(int i = 1; i < buf.Length; ++i)
            {
                if ((buf[i].Length == 1 && buf[i].First() == '-') && 
                    (buf[i - 1].Length == 1 && (Constants.AllowMathOperations.Contains(buf[i - 1].First()) || buf[i - 1].First() == '(')))
                    buf = buf.Take(i).Append("-1").Append("*").Concat(buf.Skip(i + 1)).ToArray();
            }
            return buf;
        }

        private static bool IsPlusOrMinusOperator(string lexem) => lexem.Length == 1 && (lexem.First() == '-' || lexem.First() == '+');

        private static string PreparationOfExpression(string exp) => exp.Replace(" ", string.Empty);

        private static bool FirstCheckExpression(string sourceStr) => 
            sourceStr.All(i => char.IsDigit(i) || Constants.AllowInfixOperations.Contains(i)) &&
            sourceStr.Count(i => i == '(') == sourceStr.Count(i => i == ')');

        private static byte GetPriority(string op)
        {
            if (op == "(" || op == ")")
                return 3;
            else if (op == "*" || op == "/")
                return 2;
            else if (op == "+" || op == "-")
                return 1;
            return 0;
        }

        /// <summary>
        /// This is a extended of standart Split, but contain also separators. 
        /// </summary>
        /// <param name="src">Splitable string</param>
        /// <param name="separator">Array of separators</param>
        /// <returns></returns>
        private static string[] SplitExt(this string src, char[] separator)
        {
            var buf = string.Empty;
            var res = new List<string>();
            foreach (var c in src)
            {
                if (separator.Contains(c))
                {
                    if (buf != string.Empty)
                    {
                        res.Add(buf);
                        buf = string.Empty;
                    }
                    res.Add(c.ToString());
                }
                else
                    buf += c;
            }
            if (buf != string.Empty)
                res.Add(buf);
            return res.ToArray();
        }

        #endregion
    }
}
