﻿using System.Linq;

namespace Calculator
{
    public static class Constants
    {
        private static char[] _allowMathOperations;
        public static char[] AllowMathOperations => _allowMathOperations ?? (_allowMathOperations = new[] { '+', '-', '*', '/' });

        private static char[] _allowPostfixOperations;
        public static char[] AllowPostfixOperations => _allowPostfixOperations ?? (_allowPostfixOperations = AllowMathOperations);

        private static char[] _allowInfixOperations;
        public static char[] AllowInfixOperations => _allowInfixOperations 
            ?? (_allowInfixOperations = AllowMathOperations.Concat(new[] { '(', ')' }).ToArray());
    }
}
